# REACT TEST
============================

# Features
* ReactJs
* Redux
* Firebase

# Motivation

Appraise the developer's skills by using the framework ReactJs

# Installation

Note: follow the steps to see in operation. You must have Nodejs and Npm installed

1. Clone the project 
```
git clone git@gitlab.com:shades3002/react-pratice.git
```
2. Install: 
```$ cd react-pratice
$ npm install
$ npm start```
3. Open Browser:
[http://localhost:3000](http://localhost:3000)

# Note: ```To use the application correctly you must have internet access```

# Contributors
- Carlos Gutierrez