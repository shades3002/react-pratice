export const DateToday = (() => {
    let today = new Date();
    return today.getDate() + '/' + (today.getMonth() + 1) + '/' + today.getFullYear();
})();


export const TextLabel = (arg) => {
    let string = String(arg);
    if( string.search('@react.com') !== '-1' ) {
        string = string.replace('@react.com','');
    }
    if( string.search('_') !== '-1' ) {
        string = string.replace('_',' ');
    }
    return string;
};

export const TextEmail = (arg) => {
    let string = String(arg);
    string = (string.replace(' ','_')+'@react.com').toLowerCase();
    return string;
};