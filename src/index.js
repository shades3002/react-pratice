import React from 'react';
import ReactDOM from 'react-dom';
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import { Router, Route, browserHistory } from 'react-router';
import * as firebase from  'firebase';
import { firebaseApp } from './firebase';
import { logUser } from './actions';
import reducer from './reducers';
import App from './components/App';
import LandingPage from './components/LandingPage';

const store = createStore(reducer);

firebaseApp.auth().onAuthStateChanged(user => {
    if(user) {
        firebase.database().ref('items').once('value', 
            function(snapshot) { 
                const { email } = user;
                store.dispatch(logUser(email));
                if(snapshot.numChildren() > 0) {
                    browserHistory.push('/app');
                } else {
                    browserHistory.replace('/landingpage');
                }
        });
    } else {
        browserHistory.replace('/landingpage');
    }
})

ReactDOM.render(
    <Provider store={store}>
        <Router path="/" history={browserHistory}>
            <Route path="/app" component={App} />
            <Route path="/landingpage" component={LandingPage} />
        </Router>
    </Provider>, document.getElementById('root')
)