import React, { Component } from 'react';
import { itemsCustomers, itemsApproval } from '../firebase';
import InputMask from 'react-input-mask';
import cancelInactive from '../assets/img/reject_inactive.png'; 
import cancel from '../assets/img/reject_active.png'; 
import accept from '../assets/img/accept_active.png';
import acceptInactive from '../assets/img/accept_inactive.png';
import '../css/Items.css';

class Item extends Component {
    constructor(props) {
        super(props);
        this.state = {
            terminal: '',
            delete: false,
            acceptAction: acceptInactive,
            accept: false
        }
    }
    
    setAnimation(arg) {
       this.props.onClick(arg);
    }

    approvalItem() {
        if(this.state.accept) {
        this.setAnimation('');
            const { terminal } = this.state;
            const { serverKey, name, cuit, number, date } = this.props.item;
            itemsCustomers.child(serverKey).remove();
            itemsApproval.push({name, cuit, number, date, terminal});
            this.setState({ 
                terminal: '',
                acceptAction: acceptInactive,
                accept: false
            }); 
        }
    }

    deleteItem() {
        this.setAnimation('Del');
        itemsCustomers.child(this.props.item.serverKey).remove();
        this.setState({delete: false});
    }

    refuseItem() {
        this.setState({delete: true});
    }

    cancelOperation() {
        this.setState({delete: false});
    }

    terminalValue(enter) {
        let terminal = this.state.terminal;
        if(terminal > 0 && terminal.trim().substr(0,7).length === 7) {
            this.setState({
                acceptAction: accept,
                accept: true
            });
            if( enter === true ) {
                this.approvalItem();
            }
        } else {
            this.setState({
                acceptAction: acceptInactive,
                accept: false
            });
        }
    }

    render() {
        const { name, cuit, number, date } = this.props.item;
        return ( 
            <div className="items col-md-12 center-block">
                { !this.state.delete ?
                    <div className="item">
                        <div className="cancel">
                            <a 
                                onClick={() => this.refuseItem()}
                            >
                                <img src={cancelInactive} alt="Cancelar"/>
                            </a>
                        </div>
                        <div className="name">
                            <p className="title">{name}</p>
                            <p className="cuit">CUIT: <span>{cuit}</span></p>
                        </div>
                        <div className="description">
                            <p className="establishment">Nº de establecimiento</p>
                            <p className="establishment-number">{number} </p>
                            <div className="establishment-container-date">
                                <span className="establishment-date"> {date}</span>
                            </div>
                        </div>
                        <div className="operation">
                            <div className="container-input" >
                                <InputMask 
                                    className="add-input" 
                                    {...this.props} 
                                    mask="99999999" 
                                    maskChar=" " 
                                    placeholder="Nº de Terminal"
                                    value= {this.state.terminal}
                                    onChange={event => {this.setState({terminal: event.target.value}); this.terminalValue(null)}}
                                    onKeyPress={event => {
                                        if (event.key === 'Enter') {
                                            this.terminalValue(true)
                                        }
                                    }}
                                />
                            </div>
                            <div className="add-button">
                                <a
                                    onClick={() => this.approvalItem()}
                                >
                                    <img 
                                        src={this.state.acceptAction} 
                                        alt="Aceptar"
                                    />
                                </a>
                            </div>
                        </div>
                    </div>
                    :
                    <div className="item">
                        <div className="cancel">
                            <a
                                onClick={() => this.cancelOperation()}
                            >
                                <img src={cancel} alt="Cancelar"/>
                            </a>
                        </div>
                        <div className="item-delete">
                            <span>¿Estás seguro de rechazar la solicitud de este comercio ?</span>
                            <button 
                                className="operation-button cancel-action"
                                onClick={() => this.cancelOperation()}
                            >
                                Cancelar
                            </button>
                            <button 
                                className="operation-button refuse-action"
                                onClick={() => this.deleteItem()}
                            >
                                Rechazar
                            </button>
                        </div>
                    </div> 
                } 
            </div>
        )
    }     
}

export default Item;