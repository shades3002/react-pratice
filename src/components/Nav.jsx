import React, { Component } from 'react';
import { Modal, Button } from 'react-bootstrap'; 
import { itemsCustomers } from '../firebase';
import InputMask from 'react-input-mask';
import { DateToday } from '../helpers';
import { ToastContainer } from "react-toastr";
import '../css/Nav.css';
import logo from '../assets/img/logo_visa.png';


class Nav extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showModal: false,
            name: '',
            cuit: '',
            number: '',
            date: DateToday
        };
    }

    addItems() {
        const { name, cuit, number, date } = this.state;
        if(name!=='' && cuit!=='' && number!=='' 
            && cuit.length === 11 && number.length === 9) {
            this.close();
            itemsCustomers.push({name, cuit, number, date});
            this.setState({
                name: '',
                cuit: '',
                number: '',
                date: DateToday
            });
        }else{
            this.container.error('Hay campos vacios o error en la longitud.!');
        }
    }

    close() {
        this.setState({ showModal: false })
    }
    
    open() {
        this.setState({ showModal: true })
    }

    render() {
        return (
            <header>
                <ToastContainer
                      ref={ref => this.container = ref}
                      className="toast-top-right"
                />
                <nav className="menu ">
                    <div className="menu-left">
                        <div className="logo">
                            <img src={logo} alt="Logo Visa"/>
                        </div>
                        <div className="line" >
                            <hr />
                        </div>
                        <div className="requests">
                            <span>Solicitudes</span>
                        </div>
                    </div>
                    <div className="menu-right">
                        <div className="create-request">
                            <a onClick={this.open.bind(this)} >
                                <span className="shape-add">+</span>
                                <span className="text-add">Crear solicitud</span>
                            </a>
                        </div>
                    </div>
                </nav>
                <Modal 
                    {...this.props}
                    backdrop={'static'} 
                    show={this.state.showModal} onHide={this.close.bind(this)}
                >
                    <Modal.Header>
                        <Modal.Title>
                            <span className="modal-title">Crear solicitud</span>
                        </Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <div className="form">
                            <div className="form-group form-init col-xs-12 col-md-12">
                                <label 
                                    className="input-label"
                                    htmlFor="name_">
                                    Razón Social
                                </label>
                                <input
                                    id="name_"
                                    type="text"
                                    placeholder="ej: Nexus S.A."
                                    className="form-control input-name"
                                    onChange={event => this.setState({name: event.target.value})}
                                />
                            </div>
                            <div className="form-group col-xs-12 col-sm-12 col-md-6">
                                <label 
                                    className="input-label" 
                                    htmlFor="cuit_">
                                    Número de CUIT
                                </label>
                                <InputMask 
                                    className="form-control input-cuit" 
                                    id="cuit_"
                                    mask="99-999999-9" 
                                    maskChar="" 
                                    placeholder="00-000000-0"
                                    onChange={event => this.setState({cuit: event.target.value})}
                                />
                            </div>
                            <div className="form-group col-xs-12 col-md-6">
                                <label 
                                    className="input-label" 
                                    htmlFor="number_">
                                    Número de establecimiento
                                </label>
                                <InputMask 
                                    className="form-control input-cuit" 
                                    id="number_"
                                    mask="9999999-9" 
                                    maskChar="" 
                                    placeholder="0000000-0"
                                    onChange={event => this.setState({number: event.target.value})}
                                />
                            </div>
                        </div>
                    </Modal.Body>
                    <Modal.Footer>
                        <hr className="modal-line" />
                        <div className="btn-actions">
                            <Button onClick={this.close.bind(this)}
                                className="btn-cancel"
                            >
                                Cancelar
                            </Button>
                            <Button 
                                className="btn-accept"
                                onClick={() => this.addItems()}
                            >
                                Crear
                            </Button>
                        </div>
                    </Modal.Footer>
                </Modal>
            </header>
        )
    }
}

export default Nav;