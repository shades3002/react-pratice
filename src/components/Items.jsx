import React, { Component } from 'react';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import { connect } from 'react-redux';
import { itemsCustomers } from '../firebase';
import { setItems } from '../actions';
import Item from './item';

class Items extends Component {
    constructor(props) {
        super(props);
        this.state = {
            anim: 'anim'
        };
        this.setAnimation = this.setAnimation.bind(this);
    }

    componentDidMount() {
        itemsCustomers.on('value', val => {
            let items = [];
            val.forEach(item => {
                const { name, cuit, number, date } = item.val();
                const serverKey = item.key;
                items.push({ name, cuit, number, date,  serverKey });
            })
            this.props.setItems(items);
        })
    }

    setAnimation(arg) {
        if(arg === 'Del') {
            this.setState(function(prevState, props){
                return {anim:'anim_'}
             });
        } else {
            this.setState(function(prevState, props){
                return {anim:'anim'}
            });
        }
        console.log(this.state.anim);
    }

    render() {
        return(
            <div className="row">
                <ReactCSSTransitionGroup 
                    transitionName={this.state.anim} 
                    transitionAppear={true}
                    transitionEnter={true}
                    transitionLeave={true}
                    transitionAppearTimeout={2000}
                    transitionEnterTimeout = {2000} 
                    transitionLeaveTimeout = {2000}
                >
                {   
                    this.props.items.map((item, index) => {
                        return (
                            <Item key={index} item={item} 
                                onClick={this.setAnimation.bind(this)}
                            />
                        )
                    })
                }
                </ReactCSSTransitionGroup >
            </div>
        )
    }
}

function mapStateToProps(state) {
    const { items } = state;
    return {
        items
    }
}

export default connect(mapStateToProps, { setItems })(Items);




