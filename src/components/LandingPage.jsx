import React, { Component } from 'react';
import { connect } from 'react-redux';
import { firebaseApp } from '../firebase';
import { itemsCustomers } from '../firebase';
import InputMask from 'react-input-mask';
import { DateToday, TextLabel, TextEmail } from '../helpers';
import { browserHistory } from 'react-router';
import '../css/LandingPage.css';
import logoOpacity from '../assets/img/Combined_Shape.png';
import logoDebit from '../assets/img/logo_debito_visa2.png';
import logoChip from '../assets/img/chip.png';
import arrow from '../assets/img/signo.png';


class LandingPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
            name: '',
            cuit: '',
            number: '',
            date: DateToday,
            error: {
                message: ''
            },
            valid: false
        };
    }

    SingUp() {
        const { email, password } = this.state;
        firebaseApp.auth().createUserWithEmailAndPassword(TextEmail(email), password)
            .catch(error => {
                if(error.code === "auth/email-already-in-use") {
                    firebaseApp.auth().signOut();
                    this.SingIn();
                } else {
                    this.setState({
                        error: error.code
                    })
                }
            })
    }

    SingIn() {
        const { email, password } = this.state;
        firebaseApp.auth().signInWithEmailAndPassword(email, password)
            .catch(error => {
                this.setState({
                    error
                })
            })
    }

    addItems() {
        const { name, cuit, number, date } = this.state;
        itemsCustomers.push({name, cuit, number, date});
        browserHistory.replace('/app');
    }

    validInputs() {
        if(this.state.name!=='' && this.state.cuit!=='' 
            && this.state.cuit.trim().length === 11 && this.state.number!=='' 
                && this.state.number.trim().length === 7 && this.props.user.email) {
            this.setState({valid: true});
        } else{ 
            this.setState({valid: false});
        }
    }

    render() {
        return (
            <div className="container">
                <div className="landing">
                    <div className="header">
                        <div className="row fullwidth">
                            <div className="col-md-12">
                                <div className="welcome">
                                    <span>Bienvenido.</span>
                                </div>
                                <div className="welcome-name">
                                    {
                                        this.props.user.email ?
                                            <div className="welcome-name-label">
                                                <span>{TextLabel(this.props.user.email)}</span>
                                            </div>
                                        :
                                            <div>
                                                <input
                                                    type="text"
                                                    placeholder=" Introduce tu nombre"
                                                    onChange={event => this.setState({email: event.target.value, password: event.target.value})}
                                                />
                                                <button
                                                    type="button"
                                                    onClick={() => this.SingUp()}
                                                >
                                                    <img src={arrow} alt="Button arrow"/>
                                                </button>
                                            </div>
                                     }
                                    <hr/>
                                    <div>{this.state.error.message}</div>
                                </div>
                            </div>
                            <div className="welcome-logo">
                                <img src={logoOpacity} alt="Visa" />
                                <span>Solicitudes</span>
                            </div>
                        </div>
                        <div className="row fullwidth_">
                            <div className="col-sm-12 col-md-6">
                                <div className="_card">
                                    <div className="_card-debit">
                                        <img src={logoDebit} alt="Logo debito Visa"/>
                                    </div>
                                    <div className="_card-number">
                                        <span>4546 2132 5465 7852</span>
                                    </div>
                                    <div className="_card-name">
                                        <span>NOMBRE DEL<br />CLIENTE</span>
                                    </div>
                                    <div>
                                        <img src={logoChip} className="_card-debit-chip" alt="Chip"/>
                                    </div>
                                    <div className="_card-date">
                                        <span>03/18</span>
                                    </div>
                                </div>
                            </div>
                            <div className="col-sm-12 col-md-6">
                                <div className="_main-form">
                                    <div className="form">
                                        <div className="_main-form-title col-xs-12">
                                            <span>CREA TU PRIMERA SOLICITUD</span>
                                        </div>
                                        <div className="form-group _main-form-body col-xs-12">
                                            <label
                                                className=""
                                                htmlFor="name_">
                                                Razón social del comercio
                                            </label>
                                            <input
                                                id="name_"
                                                type="text"
                                                placeholder="Razón social del comercio"
                                                className="form-control"
                                                onChange={event => {this.setState({name: event.target.value}); this.validInputs()}}
                                            />
                                        </div>
                                        <div className="form-group _main-form-body col-xs-12">
                                            <label
                                                className=""
                                                htmlFor="cuit">
                                                Número de CUIT
                                            </label>
                                            <InputMask 
                                                className="form-control _main-form-input_" 
                                                name="cuit"
                                                mask="99-999999-9" 
                                                maskChar="" 
                                                placeholder="ej: 00-000000-0"
                                                onChange={event => {this.setState({cuit: event.target.value.trim()}); this.validInputs()}}
                                            />
                                        </div>
                                        <div className="form-group _main-form-body col-xs-12">
                                            <label
                                                className=""
                                                htmlFor="number">
                                                Número de establecimiento
                                            </label>
                                            <InputMask 
                                                className="form-control _main-form-input_" 
                                                id="number"
                                                mask="9999999-9" 
                                                maskChar="" 
                                                placeholder="ej: 0000000-0"
                                                onChange={event => {this.setState({number: event.target.value.trim()}); this.validInputs()}}
                                            />
                                        </div>
                                        <div className="_main-form-footer">
                                            <button
                                                disabled={!this.state.valid}
                                                onClick={this.addItems.bind(this)}
                                            >
                                                Crear Solicitud
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="_main">
                        <div className="row">
                            <div className="col-md-12">
                                <div className="_main-title">
                                    <span>Comienza a aprobar números de establecimiento</span>
                                </div>
                                <div className="_main-description">
                                    <p>
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus luctus quis orci eget pharetra. Pellentesque lacinia ultrices arcu, quis pulvinar eros iaculis in. Duis ut aliquam felis. 
                                    </p>
                                    <br/>
                                    <p>
                                        Donec ut tellus et leo vestibulum lobortis. Aenean dignissim varius est, nec porttitor augue aliquam vitae. Vivamus placerat nunc eu placerat maximus. Morbi tincidunt nunc eu elit porta, vel consequat leo varius. Vivamus vel ornare odio, eget feugiat purus. Aliquam erat volutpat. Vivamus a est blandit, rutrum mauris convallis, facilisis sem. Pellentesque.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    const { user } = state;
    return {
        user
    }
}
export default connect(mapStateToProps, null)(LandingPage);