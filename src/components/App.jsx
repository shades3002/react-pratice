import React, { Component } from 'react';
import { connect } from 'react-redux';
import '../css/App.css';
import Nav from './Nav';
import Items from './Items';

class App extends Component {
    render() {
        return (
            <div className="container">
                <Nav />
                <section className="main">
                    <Items />
                </section>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {}
}

export default connect(mapStateToProps, null)(App);