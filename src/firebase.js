import * as firebase from  'firebase';

const config = {
    apiKey: "AIzaSyASE7hxNtX6hsX06XWAAduQd9IUEgRHI-0",
    authDomain: "react-practice-80298.firebaseapp.com",
    databaseURL: "https://react-practice-80298.firebaseio.com",
    projectId: "react-practice-80298",
    storageBucket: "",
    messagingSenderId: "427882849152"
};

export const firebaseApp = firebase.initializeApp(config);
export const itemsCustomers = firebase.database().ref('items');
export const itemsApproval = firebase.database().ref('itemsApproval');